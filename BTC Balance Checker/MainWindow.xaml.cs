﻿using Microsoft.Win32;
using System;
using System.IO;
using System.Linq;
using System.Windows;
using System.ComponentModel;
using System.Net;
using Newtonsoft.Json.Linq;
using System.Runtime.InteropServices;

namespace BTC_Balance_Checker
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private BackgroundWorker bw = new BackgroundWorker();
        private string filePath, fileOutput, lastPublic;
        private int lineCount, counter, success, lastLineNumber;
        private int mTbSleep;

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        static extern EXECUTION_STATE SetThreadExecutionState(EXECUTION_STATE esFlags);

        public enum EXECUTION_STATE : uint
        {
            ES_AWAYMODE_REQUIRED = 0x00000040,
            ES_CONTINUOUS = 0x80000000,
            ES_DISPLAY_REQUIRED = 0x00000002,
            ES_SYSTEM_REQUIRED = 0x00000001
        }

        public MainWindow()
        {
            InitializeComponent();

            bw.WorkerReportsProgress = true;
            bw.WorkerSupportsCancellation = true;
            bw.DoWork += new DoWorkEventHandler(bw_DoWork);
            bw.ProgressChanged += new ProgressChangedEventHandler(bw_ProgressChanged);
            bw.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bw_RunWorkerCompleted);
        }

        private void ButtonBrowse_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.InitialDirectory = @"D:\";
            openFileDialog.Title = "Browse Text Files";
            openFileDialog.CheckFileExists = true;
            openFileDialog.CheckPathExists = true;
            openFileDialog.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
            openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

            if (openFileDialog.ShowDialog() == true)
            {
                textBoxFilePath.Text = openFileDialog.FileName;
                filePath = textBoxFilePath.Text;

                // write file path to the Settings
                Properties.Settings.Default.filePath = filePath;
                Properties.Settings.Default.Save();
            }
        }

        private void ButtonStart_Click(object sender, RoutedEventArgs e)
        {
            // prevent sleep mode
            SetThreadExecutionState(EXECUTION_STATE.ES_CONTINUOUS | EXECUTION_STATE.ES_SYSTEM_REQUIRED);

            if (!String.IsNullOrEmpty(tbSleep.Text.ToString()))
            {
                mTbSleep = Int32.Parse(tbSleep.Text.ToString());
            }
            else
            {
                mTbSleep = 500;
            }

            // read file path from Settings
            if (String.IsNullOrEmpty(filePath))
            {
                string filePathSettings = Properties.Settings.Default.filePath;
                if (!String.IsNullOrEmpty(filePathSettings) || !filePathSettings.Equals(""))
                {
                    filePath = filePathSettings;
                    textBoxFilePath.Text = filePath;
                }
            }

            string dirPath;

            try
            {
                dirPath = Path.GetDirectoryName(filePath);
                string fileOutput1 = Path.Combine(dirPath, "balance.csv");
                fileOutput = dirPath + "\\balance.csv";
            } catch (ArgumentException)
            {
                this.tbProgress.Text = "Error: wrong file path";
            }

            FindLastLine();

            if (bw.IsBusy != true)
            {
                buttonBrowse.IsEnabled = false;
                buttonStart.IsEnabled = false;
                bw.RunWorkerAsync();
            }
        }

        private void ButtonCancel_Click(object sender, RoutedEventArgs e)
        {
            if (bw.WorkerSupportsCancellation == true)
            {
                buttonBrowse.IsEnabled = true;
                buttonStart.IsEnabled = true;
                bw.CancelAsync();
            }
        }

        private void FindLastLine()
        {
            // Read the last line of the output file
            string lastLine = "";
            string line;
            lastLineNumber = 0;
            int counter = 0;

            // Read the file
            if (File.Exists(fileOutput))
            {
                System.IO.StreamReader file = new System.IO.StreamReader(@fileOutput);
                while ((line = file.ReadLine()) != null)
                {
                    lastLine = line;
                }
                file.Close();
            }

            string[] strArray = lastLine.Split(';');
            lastPublic = strArray[0];

            // Read the init file 
            if (File.Exists(filePath) && !string.IsNullOrEmpty(lastPublic))
            {
                System.IO.StreamReader file = new System.IO.StreamReader(@filePath);

                while ((line = file.ReadLine()) != null)
                {
                    if (line.Equals(lastPublic))
                    {
                        lastLineNumber = counter;
                        break;
                    }
                    counter++;
                }
                file.Close();
            }
        }

        private void bw_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;

            // Perform a time consuming operation and report progress.
            counter = 0;
            string line;
            lineCount = File.ReadLines(@filePath).Count();

            string url = "https://blockchain.info/rawaddr/";
            success = 0;

            bool check = false;

            // Read the file  
            System.IO.StreamReader file = new System.IO.StreamReader(@filePath);

            while ((line = file.ReadLine()) != null)
            {

                if ((worker.CancellationPending == true))
                {
                    e.Cancel = true;
                    break;
                }

                if (lastLineNumber == 0 && counter == 0)
                {
                    check = true;
                } else if (counter > lastLineNumber)
                {
                    check = true;
                } else
                {
                    check = false;
                }

                //if (lastLineNumber > 0 && counter > lastLineNumber)
                //if (lineCount > 0)
                if (lineCount > 0 && check)
                {
                    var json = new WebClient().DownloadString(url + line);

                    JObject token = JObject.Parse(json);

                    int finalBalance = (int)token.SelectToken("final_balance");
                    string newLine = line + ";" + finalBalance.ToString();

                    if (finalBalance != 0)
                    {
                        success++;
                    }

                    using (System.IO.StreamWriter file1 = new System.IO.StreamWriter(@fileOutput, true))
                    {
                        file1.WriteLine(newLine);
                    }

                    int percentage = (int)(counter * 100 / lineCount);
                    System.Threading.Thread.Sleep(mTbSleep);
                    worker.ReportProgress(percentage);
                }
                counter++;
            }
            file.Close();
        }

        private void bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if ((e.Cancelled == true))
            {
                this.tbProgress.Text = "Canceled!";
            }
            else if (!(e.Error == null))
            {
                this.tbProgress.Text = ("Error: " + e.Error.Message);
                buttonBrowse.IsEnabled = true;
                buttonStart.IsEnabled = true;
            }
            else
            {
                this.tbProgress.Text = "Done!";
            }
        }

        private void bw_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            this.tbProgress.Text = (e.ProgressPercentage.ToString() + "%");
            this.tbProgressLine.Text = (counter.ToString() + " from " + lineCount.ToString());
            if (success != 0)
            {
                this.tbResult.Text = (success.ToString());
            }
        }
    }
}
